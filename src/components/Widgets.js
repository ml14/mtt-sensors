import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import   '../css/widgets.css';
import Grid from '@material-ui/core/Grid';

const API = process.env.REACT_APP_REST_URL

export default class Widgets extends Component {

    constructor(props){
        super()
        this.state =  {
            sensors: null,
             loading: true,
             message:null
            }

        setTimeout( () => this.tick(), Math.floor(Math.random() * 100))
    }

  componentDidMount(){
      this.mounted = true // acciona  setSstate no tick
      this.intervalID = setInterval( () => this.tick(), 3000)
  }


  componentWillUnmount(){
      this.mounted = false
      clearInterval(this.intervalID)
  }

    tick(){
        let query = API + 'realtime'
        fetch(query)
        .then(response => response.json())
        .then(data => {if (this.mounted) 
               this.setState({tags: data, loading:false})}
        )
        .catch( error => { if (this.mounted)
             this.setState( {loading:true, message: error} )})
    }



    render(){
        const {tags} = this.state
       const monitors = this.props.monitors
        if (this.state.message){
            return <div>#ERROR WAIT</div>
        }
        if (this.state.loading){
         return   <div><CircularProgress/></div>
            
        }
        // null values
        if (Object.entries(tags).length === 0) {
            return null
        }
          //map
        const items = monitors.map(  id => <Grid item xs={3} key={id.tag.toString()}>
                    <div className='box'>
                    <div className='text'>{id.text}</div>
                    <div className='value'>{tags[id.tag] ? tags[id.tag].toFixed(id.digits): '----'}</div>
                    <div className='unit'>{id.unit ? id.unit : 'I/O'}</div>
                </div>
            </Grid>
          )
        return(
           <Grid container>
                 {items}
           </Grid>    
          
        )
    }
}