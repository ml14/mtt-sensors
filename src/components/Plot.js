import React, {Component} from 'react';
import {ResponsiveContainer,Tooltip,AreaChart,Area, XAxis,YAxis,CartesianGrid,LineChart,Line, CartesianAxis } from 'recharts';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';


const API = process.env.REACT_APP_REST_URL

export default class Plot extends Component {

    constructor(props){
        super()
        this.state =  {
            sensors: null,
             loading: true,
             message:null
            }

        setTimeout( () => this.tick(), Math.floor(Math.random() * 100) + 100)
    }
    componentDidMount(){
        this.mounted = true // acciona  setSstate no tick
        this.intervalID = setInterval( () => this.tick(), 30000) //30 sec
    }
  
  
    componentWillUnmount(){
        this.mounted = false
        clearInterval(this.intervalID)
    }
  

    tick(){
        let query = API + 'history?sensor=temperature';
        fetch(query)
        .then(response => response.json())
        .then(result => {if (this.mounted) 
               this.setState({data: result, loading:false})}
        )
        .catch( error => { if (this.mounted)
             this.setState( {loading:true, message: error} )})
    }


render(){
     const { data } =  this.state 
        if (this.state.message){
            return <div>#ERROR WAIT</div>
        }
        if (this.state.loading){
         return   <div><CircularProgress/></div>
            
        }
        // null values
        if (Object.entries(data).length === 0) {
            return null
        }

       
    return (
       <div style={{width:'100%' , height:200}}>
            <ResponsiveContainer>
                <AreaChart data = {data} >
                    <Tooltip />
                    <CartesianAxis strockDasharray="3 3"></CartesianAxis>
                    <YAxis></YAxis>
                    <XAxis dataKey='timestamp' minTickGap={30}
                    tickFormatter = { (unitTime) => moment(unitTime).format('HH:mm')}
                    > </XAxis>
                    
                    <Area dataKey="value" />
                </AreaChart>
          </ResponsiveContainer>
        </div>
    )
}




}