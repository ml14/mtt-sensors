import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: theme.spacing(1),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));
 var today = new Date().toLocaleString("sv-SE" ,
 {timeZone: "Europe/Lisbon"}).slice(0,10)
export default function DatePickers() {
  const classes = useStyles();

// function , ready to present,  
  const handlerChange = (event) => {


  }
  return (
    <form className={classes.container} noValidate>
      <TextField
        id="date"
        label="Filter Graph"
        type="date"
        defaultValue={today}
        className={classes.textField}
        onChange={handlerChange}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </form>
  );
}