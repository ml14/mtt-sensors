import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('test for app name', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/mtt-sensors/i);
  expect(linkElement).toBeInTheDocument();
  
});
