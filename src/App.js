import { Grid } from '@material-ui/core';
import React from 'react';
import DatePicker from './components/DatePicker';
import Plot from './components/Plot';
import Widgets from './components/Widgets';


var sensors = [   
  {text:'Temperatura' ,tag:'temperature', unit:'\u00b0C', digits:'1'},
  {text:'Humidity', tag:'humidity' ,unit:'%', digits:'1'},
  {text:'Light', tag:'light', unit:'lux', digits:'1'}
]
export default function App() {

  return(
    <div>
      <Widgets monitors={sensors} />
      <Grid container spacing={3}>
        <Grid item xs={6} >
          <Plot />
        </Grid>
        <Grid item xs={2} >
          {process.env.REACT_APP_VERSION}
        </Grid>
        <Grid item xs={2} >

          {process.env.REACT_APP_NAME}
          <Grid item xs={2} >


</Grid>
        </Grid>
       <DatePicker />
      </Grid>
    </div>
  )
}


