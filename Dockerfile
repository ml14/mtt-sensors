FROM node

WORKDIR /root

COPY package.json /root/package.json
RUN npm install react-scripts -g --silent
RUN npm install -g serve
COPY . /root 
EXPOSE 5000
CMD ["serve", "-S","build"]
